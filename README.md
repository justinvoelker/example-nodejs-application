# Example Node.js Application

An example Node.js application built with helpful defaults for new projects.

Includes:

- TypeScript
- ESLint
- Prettier
- Jest
- GitLab CI/CD
- Docker

Optional:

- [commitlint](https://github.com/conventional-changelog/commitlint) Ensure commit messages meet the the Convention Commits format.
- [lint-staged](https://github.com/okonet/lint-staged) Automatically run linters against staged files.

# Developing in Docker

Development of a Node.js application can either take place directly on a host machine running Node.js or within a Docker container running Node.js. While installing Node.js directly on the host machine may simplify some aspects of development, it can complicate others by requiring the careful coordination of different Node.js versions, additional packages, etc. Developing within a Docker container introduces an additional layer of complexity but the isolation from the host machine can be worth.

All of the commands in the _Project creation steps_ section below must be executed from either a host running Node.js or a Docker container running Node.js. The simplest way to run Node.js in a Docker container is to use an official Node.js Docker image. For example, executing `docker run -it --rm node:lts-alpine sh` will take you to the shell of a temporary Node.js container. Typing `exit` will exit the shell which will stop the container.

Although running Node.js inside a Docker container is straightforward, some development tasks require additional packages or setup. For example, the standard-version package requires git to automatically create a release. As another example, test runners must be configured to execute tests inside a container running Node.js. The easiest solution is to extend one of the official Node.js Docker images to include any development requirements. For the aforementioned examples, the only new requirement is that git be installed and configured. The following **Dockerfile** installs git into a Node.js image and configures it for the desired user.

```
FROM node:lts-alpine
RUN apk add --no-cache git && \
    git config --system user.name "John Doe" && \
    git config --system user.email "john.doe@example.com"
```

To build this image, execute `docker build -t node-dev .`. Once built, start a temporary container from that image by executing `docker run -it --rm node-dev sh`. Finally, in the running container, execute `git config --list` and notice the user name and email address. Using this new image as a basis for developing Node.js projects ensures access to additional tooling required during development.

# Project creation steps

> Note, this Example Node.js Application was created by following the steps below. This project can either be cloned as a starting point for a new project or the steps below can be executed to recreate the project exactly as it exists in this repository.

> Note, all of the commands in this project must be executed from either a host running Node.js or a Docker container running Node.js. Until a **docker-compose.yml** file is created, commands can be executed in a temporary Docker container by executing ``docker run -it --rm -u 1000 -w /usr/src/app -v `pwd`/data/app/app:/usr/src/app node-dev sh``. This command executes as user 1000, sets a working directory, and mounts the current host directory into the working directory.

## Initialize project

Execute `yarn init` and answer the prompts as necessary for the project.

Create a **.gitignore** file with the following content:

```
# dependencies
node_modules

# production
dist

# misc
yarn-debug.log*
yarn-error.log*
```

## Add TypeScript

Install necessary TypeScript packages and initialize the TypeScript config file.

```
yarn add --dev nodemon ts-node typescript
npx tsc --init
```

Create a **src/index.ts** file with the following content:

```
function execute() {
  console.log(new Date());
}
setInterval(execute, 1000);
```

Update the **tsconfig.json** file with the following changes:

- Uncomment **outDir** and change the value to `dist`
- Uncomment **rootDir** and change the value to `src`
- After the **compilerOptions** object, add `"exclude": [ "node_modules" ]` and `"include": [ "src/**/*" ]`

Update the **package.json** file to add the following:

```
"scripts":{
  "watch": "nodemon src/index.ts",
  "build": "npx tsc"
}
```

Execute `yarn run watch` to see the output of the running Node.js application. The console log should print the current date and time every second.

Update the **src/index.ts** file to an interval of 5000 and save the file. Notice that nodemon will restart the application and the console log now only prints the date and time every 5 seconds.

Press Ctrl+C to break out of the running application and return to the console.

To build the application, excute `yarn run build` and notice the creation of a new **dist** directory. This **dist** directory can be deleted as it will not be committed into the project repository and is only ever build by the CICD pipeline for use in the published project.

## Create Docker Compose file

Now that a simple, working Node.js application exists, Docker Compose can be used to manage the container rather than command-line docker commands. Exit/stop the current temporary container and then create a Docker Compose file.

Create a **docker-compose.yml** file with the following content:

```
version: '3.4'
services:
  app:
    image: node-dev
    container_name: example-nodejs-application
    restart: always
    working_dir: /usr/src/app
    command: ["yarn", "watch"]
    volumes:
      - ./data/app/app/:/usr/src/app
```

Now, to run the container containing the application, execute `docker-compose up -d` from the host machine. The console log output can be viewed with `docker-compose logs -f` and the container can be stopped with `docker-compose down`.

## Add ESLint and Prettier

ESLint and Prettier are added together since interact with each other.

Install ESLint, Prettier, and related TypeScript packages.

```
yarn add --dev @typescript-eslint/eslint-plugin @typescript-eslint/parser eslint eslint-config-prettier prettier
```

Create a **.eslintrc.js** file with the following content:

```
module.exports = {
  root: true,
  env: { node: true },
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint"],
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "prettier",
  ],
};
```

Create a **.prettierrc** file with the following content:

```
{
  "arrowParens": "always",
  "bracketSpacing": true,
  "endOfLine": "lf",
  "quoteProps": "as-needed",
  "semi": true,
  "singleQuote": false,
  "tabWidth": 2,
  "trailingComma": "es5",
  "useTabs": false
}
```

Create a **.prettierignore** file with the following content:

```
README.md
tsconfig.json
```

Update the **package.json** file to add the following scripts:

```
"scripts":{
  ...
  "format": "npx prettier --write .",
  "lint": "eslint --cache ."
},
```

To see formatting and linting in action, add `var foo='bar'` to the end of the **src/index.js** file. If developing within an IDE that has an ESLint extension installed, that line of code should contain two errors: _Unexpected var, use let or const instead_ and _'foo' is assigned a value but never used_. To see these errors at the command line and fix those that can be automatically fixed, execute `yarn run lint`.

To format the file, execute `yarn run format` and notice that the equals sign is now surrounded by spaces, the string is wrapped with double quotes, and a semi-colon has been appened to the end of the line. If not already present, a trailing new line will have been added as well.

## Add Jest

Install necessary Jest packages and initialize the Jest config file.

```
yarn add --dev @types/jest jest ts-jest
yarn ts-jest config:init
```

Create a **src/math.ts** file with the following content:

```
export function sum(a: number, b: number): number {
  return a + b;
}
```

Create a **src/math.test.ts** file with the following content:

```
import { sum } from "./math";

test("sum 1 + 2 to equal 3", () => {
  expect(sum(1, 2)).toBe(3);
});
```

To test the application, excute `yarn run test` and notice the test results.

## Add standard-version

To assist with releases, the standard-version package can bump version numbers, create a changelog, commit the release, and tag the commit.

Execute the following command to install standard-version.

```
yarn add --dev standard-version
```

Update the **package.json** file to add the following script:

```
"scripts":{
  ...
  "release": "standard-version"
},
```

## Optional - Add commitlint

Install necessary commitlint packages and create the commitlint config file.

```
yarn add --dev @commitlint/cli @commitlint/config-conventional
echo 'module.exports = { extends: ["@commitlint/config-conventional"] };' > commitlint.config.js
```

Install husky and create a commit message hook to lint commits before they are created.

```
yarn add --dev husky
yarn husky install
npx husky add .husky/commit-msg 'npx --no-install commitlint --edit "$1"'
```

## Optional - Add lint-staged

Install lint-staged.

```
yarn add --dev lint-staged
```

Create a **.lintstagedrc.js** file with the following content:

```
module.exports = {
  "*.{ts,js,json}": ["eslint --cache --fix", "prettier --write"],
};
```

Create a pre-commit hook to format and lint before code is committed.

```
npx husky add .husky/pre-commit 'npx lint-staged'
```

## Add Docker

Create a **Dockerfile** file with the following content:

```
FROM node:lts-alpine AS build
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
RUN yarn run build

FROM node:lts-alpine
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn install --production
COPY --from=build /usr/src/app/dist/ ./dist
CMD [ "node", "dist/index.js" ]
```

## Add GitLab CI

When the project is updated via a push from local, a merge commit to main, or a pushed tag matching the semantic version format, tests will be executed and a new image (potentially two) will be built. Every commit to main will build a Docker image tagged with _latest_ while every pushed semantic version tag will build a Docker image tagged with that version number.

Create a **.gitlab-ci.yml** file with the following content:

```
image: docker:20.10

services:
  - docker:20.10-dind

stages:
  - test
  - build

test:
  stage: test
  image: node:lts-alpine
  script:
    - yarn install
    - yarn run test

Build latest:
  stage: build
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build --pull -t $CI_REGISTRY_IMAGE:latest .
    - docker push $CI_REGISTRY_IMAGE:latest
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'

Build version:
  stage: build
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build --pull -t $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v?(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/'
```

## Optional - Push to Docker Hub

The GitLab CI file above will publish container images to the GitLab container registry. To publish images to Docker Hub, follow the steps below.

- Log in to https://hub.docker.com
  - Go to Account Settings, Security
  - Create a new Access Token and notate it for later steps
- Log in to https://gitlab.com
  - In the desired repository, go to Settings, CI/CD
  - Expand the Variables section and add the following key/value pairs
    - `CI_REGISTRY` set to `docker.io`
    - `CI_REGISTRY_IMAGE` set to `index.docker.io/<username>/<project>`
    - `CI_REGISTRY_PASSWORD` set to the access token created above
    - `CI_REGISTRY_USER` set to the Docker username

The GitLab CI variables created above are protected by default which means they are only available to pipelines running on protected branches and protected tags. Though GitLab projects are created with a protected main branch, no tags are protected by default. To protect all tags, follow the steps below.

- Log in to https://gitlab.com
  - In the desired repository, go to Settings, Repository
  - Expand the Protected Tags section and set the following values
    - `Tag` set to `*`
    - `Allowed to create` set to `Maintainers`
  - Click Protect

Without protecting tags, pipelines executed for tags would not have access to the Docker Hub variables set above and would instead fall-back to the default GitLab variables which would publish the tagged container to the GitLab container registry.

# Configure IDE

## VSCode

To format and lint files on save, create a **.vscode/settings.json** file with the following content:

```
{
  "editor.formatOnSave": true,
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "files.watcherExclude": {
    "**/.git/objects/**": true,
    "**/.git/subtree-cache/**": true,
    "**/node_modules/**": true
  }
}
```

# Releases

## First release

A first release will create a changelog and tag without first bumping the version number specified in the **package.json**. For example, if using 0.0.0 as a version number during initial development, execute the following command to create a release whose changelog and tag indicate version 0.0.0.

```
yarn run release --first-release
```

## Regular releases

For regular releases, simply follow the [Conventional Commits Specification](https://www.conventionalcommits.org) when committing changes and, when a release is ready, execute the following command.

```
yarn run release
```

Running the previous command without arguments will automatically bump versions numbers as follows:

- _fix_ commits patch bugs and will bump the patch number (0.0.X)
- _feat_ commits introduce features and will bump the minor number (0.X.0)
- Appending an exclamation point to any commit type/scope and/or adding a footer beginning with "BREAKING CHANGE:" introduces a breaking change and will bump the major number (X.0.0)

## Force a major version

During initial development, if starting with a package version less than 1.0.0 (for example, 0.0.0), breaking changes will only increment the minor number. The only way to bump the major version to indicate a first, stable release is by executing the following command:

```
yarn run release --release-as major
```

After the first 1.0.0 release has taken place, the major number will be bumped automatically whenever a breaking change exists (or the command above is executed to force a new major version).

## Undo a release

In the event of a mistake, a release (that has not been pushed to origin) can be undone by resetting the branch and deleting the tag (where _v0.0.0_ in the following example is the automatically created release tag).

```
git reset --hard HEAD~1
git tag --delete v0.0.0
```
